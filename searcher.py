import requests
import re
import sys
from bs4 import BeautifulSoup

max_price = 6000

miles_regex = re.compile('\d{3},?\d{3}')
make_regex = re.compile('toyota|hyundai|honda|subaru|nissan|chevy|jeep|bmw|mazda')

skipped = 0
total_results = 0
hits = 0


base_url = 'https://siskiyou.craigslist.org'
next_page = '/search/cta?search_distance=100&postal=96067'

save_file = open('list.txt', 'w')
save_file.close()

while next_page != '':
    res = requests.get(base_url + next_page)
    soup = BeautifulSoup(res.text, features='html.parser')
    results = soup.select('.result-row')

    for result in results:
        total_results += 1
        print('\r Checking entry number: {0} --- Matches so far: {1}'.format(total_results, hits), flush=True, end='')

        score = 0
        url = result.select('.result-title')[0].attrs['href']
        listing_request = requests.get(url)
        listing_soup = BeautifulSoup(listing_request.text, features='html.parser')

        salvage = False

        if 'salvage' in result.getText().lower():
            salvage = True

        title = listing_soup.select('#titletextonly')[0].getText()

        if len(listing_soup.select('.price')) > 0:
            price = listing_soup.select('.price')[0].getText()
        else:
            skipped += 1
            continue

        if int(price[1:]) > max_price or int(price[1:]) < 2000:
            skipped += 1
            continue
        
        miles = -1

        spans = listing_soup.select('span')

        for span in spans:
            if span.getText().startswith('odometer'):
                miles = span.select('b')[0].getText()

        if miles == -1:        
            result = miles_regex.search(listing_soup.select('#postingbody')[0].getText())
            if result:
                mile_text = result.group(0)
                miles = mile_text.replace(',', '')

        if int(miles) < 1000:
            miles = int(miles) * 1000

        if miles and int(miles) > 150000:
            skipped += 1
            continue

        make = '???'

        if make_regex.search(title.lower()):
            make = make_regex.search(title.lower()).group(0)

        if make == '???':
            skipped += 1
            continue
        
        # TODO: Filter out manual transmission

        if int(miles) < 0:
            miles = "unknown"

        entry_string = '\r{0} -- {1} -- {2} miles                      \n'.format(title, price, miles)
        if salvage:
            entry_string += 'Warning! Possibly a salvage.\n'
        entry_string += url + '\n' + ('-' * 100) + '\n'

        print(entry_string, end='')

        save_file = open('list.txt', 'a')
        save_file.write(entry_string)
        save_file.close()

        hits += 1
    
    next_button = soup.select('.next')[0]
    next_page = ''
    if 'href' in next_button.attrs.keys():
        next_page = next_button.attrs['href']

print('Out of {0} listings, I skipped {1} because they did not match the criteria.'.format(total_results, skipped))
